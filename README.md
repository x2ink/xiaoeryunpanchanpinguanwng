# 小二云盘产品官网

## 1 项目简介

本项目使用HBuilderX创建的基于Vue3的uniapp项目  
使用框架：uniapp,vue3

## 2 使用教程

运行环境： node.js v.20.12.1  
运行工具：HBuilderX
运行教程：下载本项目之后HBuilderX点击运行到浏览器即可

## 3 项目截图

![网站首页](https://pic.imgdb.cn/item/66a8ae4dd9c307b7e9060015.png)
![网站首页](https://pic.imgdb.cn/item/66a8ae62d9c307b7e9060e4b.png)
![网站首页](https://pic.imgdb.cn/item/66a8ae7ad9c307b7e9062326.png)

## 4 提示

本项目是响应式网站,移动端请自己部署查看。[点击预览](http://www.x2.ink)
